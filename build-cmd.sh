#!/usr/bin/env bash

#cd "$(dirname "$0")"
top="$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
exec 4>&1; export BASH_XTRACEFD=4; set -x
# make errors fatal
set -e
# complain about undefined variables
set -u

PIXMAN_SOURCE_DIR=pixman
VERSION_HEADER_FILE="$top/$PIXMAN_SOURCE_DIR/configure"
version=$(sed -n -E "s/PACKAGE_VERSION='([0-9.]+)'/\1/p" "${VERSION_HEADER_FILE}")
build=${AUTOBUILD_BUILD_ID:=0}

if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

stage="$(pwd)"

# load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

echo "${version}.${build}" > "${stage}/VERSION.txt"
pushd "$top/$PIXMAN_SOURCE_DIR"
    case "$AUTOBUILD_PLATFORM" in

        # ------------------------ windows, windows64 ------------------------
        windows*)
        ;;

        # ------------------------- darwin, darwin64 -------------------------
        darwin*)
        ;;            

        # -------------------------- linux, linux64 --------------------------
        linux*)
            # Linux build environment at Linden comes pre-polluted with stuff that can
            # seriously damage 3rd-party builds.  Environmental garbage you can expect
            # includes:
            #
            #    DISTCC_POTENTIAL_HOSTS     arch           root        CXXFLAGS
            #    DISTCC_LOCATION            top            branch      CC
            #    DISTCC_HOSTS               build_name     suffix      CXX
            #    LSDISTCC_ARGS              repo           prefix      CFLAGS
            #    cxx_version                AUTOBUILD      SIGN        CPPFLAGS
            #
            # So, clear out bits that shouldn't affect our configure-directed build
            # but which do nonetheless.
            #
            unset DISTCC_HOSTS CC CXX CFLAGS CPPFLAGS CXXFLAGS

            # Prefer gcc-4.6 if available.
            #if [[ -x /usr/bin/gcc-4.6 && -x /usr/bin/g++-4.6 ]]; then
            #    export CC=/usr/bin/gcc-4.6
            #    export CXX=/usr/bin/g++-4.6
            #fi


            # Default target per autobuild build --address-size
            opts="${TARGET_OPTS:--m$AUTOBUILD_ADDRSIZE $LL_BUILD_RELEASE}"

            # Handle any deliberate platform targeting
            if [ ! "${TARGET_CPPFLAGS:-}" ]; then
                # Remove sysroot contamination from build environment
                unset CPPFLAGS
            else
                # Incorporate special pre-processing flags
                export CPPFLAGS="$TARGET_CPPFLAGS"
            fi


		# Debug first
	#        pushd "$TOP/$SOURCE_DIR"
	#		    CFLAGS="$opts -O0 -g -fPIC -DPIC" CXXFLAGS="$opts -O0 -g -fPIC -DPIC" \
	#		    ./configure --prefix="$stage" --enable-debug --includedir="$stage/include/glib" 
	#		    make
	#		    make install
	 
	       # Release last
        pushd "$top/$PIXMAN_SOURCE_DIR"

            CFLAGS="$opts" CXXFLAGS="$opts" \
		./configure --prefix="$stage" --includedir="$stage/include" --libdir="$stage/lib/release"
            make
            make install
           
	
        popd

        # conditionally run unit tests
        if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
            make test
        fi

        # clean the build artifacts
        make distclean
        ;;
    esac
    mkdir -p "$stage/LICENSES"
    #pixman doesn't have a LICENSE file. Use COPYING which has license and copyright information.
    cp -a COPYING "$stage/LICENSES/pixman.txt"
popd

mkdir -p "$stage"/docs/pixman/
cp -a ../README.Linden "$stage"/docs/pixman/
